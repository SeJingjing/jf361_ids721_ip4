use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use tracing_subscriber;

#[derive(Deserialize)]
struct Request {
    word: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    char_count: usize,
}


async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let word = event.payload.word;
    let char_count = word.chars().count();
    
    tracing::info!("word {}: {} characters", word, char_count); 


    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        char_count: char_count,
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)

}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
