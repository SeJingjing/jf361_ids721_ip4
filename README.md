# jf361_ids721_ip4

## Project Introduction
This project implements a Rust AWS Lambda and Step Functions.

## Project Description
- Rust AWS Lambda function
- Step Functions workflow coordinating Lambdas
- Orchestrate data processing pipeline

## Project Setup
#### 1. Create two Rust Lmbda functions
1. Use below code to create a sample RUST funciton.
   ```angular2html
   cargo lambda new <PROJECT-NAME>
   cd <PROJECT-NAME>
   ```
2. Implement the function in `src/main.rs` and add dependencies in `Cargo.toml`.

#### 2. Deploy to AWS
1. Create a `role` for this project and add permission `AWSLambda_FullAccess`, `AWSLambdaBasicExecutionRole`, and `IAMFullAccess`.
2. Deploy the RUST functions project to AWS.
   ```angular2html
   cargo lambda build --release
   cargo lambda deploy --region <AWS-REGION> --iam-role <ROLE-IAM>
   ```

#### 3. Setup Step Functions workflow on AWS
1. Navigaete to `Step Functions` on the AWS console, and click `Create state machine`. 
<p>
  <img src="screenshots/createSM.png" />
</p>

2. Trag twice `AWS Lambda Inovke` icon to the graph.
3. Select the deployed Lambda functions to each invoke icon.
4. Click `Create` to create the State machine.
5. To use the Step Functions workflow, click `Start execution` under the previous created State machine, and enter the data.
<p>
  <img src="screenshots/SM.png" />
</p>

## Project Description
This project implement to Rust Lambda function that one is to count the number of characters from the input word string, and another function is to determine whether a word is a long word based on the count.

## Screenshot
#### Lambda Functionality
First step function take a string and output a count
<p>
  <img src="screenshots/input.png" />
</p>
Second step function take a count and make a judgement.
<p>
  <img src="screenshots/output_s.png" />
</p>
<p>
  <img src="screenshots/output_l.png" />
</p>

#### Step Function Workflow
Below  are my step function definition
```
{
  "Comment": "A description of my state machine",
  "StartAt": "ip4Input",
  "States": {
    "ip4Input": {
      "Type": "Task",
      "Resource": "arn:aws:states:::lambda:invoke",
      "OutputPath": "$.Payload",
      "Parameters": {
        "Payload.$": "$",
        "FunctionName": "arn:aws:lambda:us-east-1:637423420619:function:ip4_input:$LATEST"
      },
      "Retry": [
        {
          "ErrorEquals": [
            "Lambda.ServiceException",
            "Lambda.AWSLambdaException",
            "Lambda.SdkClientException",
            "Lambda.TooManyRequestsException"
          ],
          "IntervalSeconds": 1,
          "MaxAttempts": 3,
          "BackoffRate": 2
        }
      ],
      "Next": "ip4Output"
    },
    "ip4Output": {
      "Type": "Task",
      "Resource": "arn:aws:states:::lambda:invoke",
      "OutputPath": "$.Payload",
      "Parameters": {
        "Payload.$": "$",
        "FunctionName": "arn:aws:lambda:us-east-1:637423420619:function:ip4_output:$LATEST"
      },
      "Retry": [
        {
          "ErrorEquals": [
            "Lambda.ServiceException",
            "Lambda.AWSLambdaException",
            "Lambda.SdkClientException",
            "Lambda.TooManyRequestsException"
          ],
          "IntervalSeconds": 1,
          "MaxAttempts": 3,
          "BackoffRate": 2
        }
      ],
      "End": true
    }
  }
}
```
Execution Events
<p>
  <img src="screenshots/execution.png" />
</p>

#### Data Processing
Input data to input function
<p>
  <img src="screenshots/inin.png" />
</p>
Output data from input function
<p>
  <img src="screenshots/inout.png" />
</p>
Input data to output function
<p>
  <img src="screenshots/outin.png" />
</p>
Output data from output function
<p>
  <img src="screenshots/outout.png" />
</p>


## Demo
The Demo video: [Demo Video.mov](Demo%20video.mov)
